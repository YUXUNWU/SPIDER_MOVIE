# coding=utf8
#头文件：导入所需库
import importlib,sys
importlib.reload(sys)#编码转换
from selenium import webdriver#自动化测试工具
import time#时间函数库：在本项目中主要用于强制时间延时
from lxml import html#Xpath支持
import requests#经典requests访问支持
import pymysql#数据库支持
#MySQL数据库链接
db = pymysql.connect(host="localhost", user="root", password="MYSQL2015190103", db="Spider_movie", port=3306, use_unicode=True, charset="utf8")
cur = db.cursor()
#启动自动化测试工具（谷歌浏览器）
driver = webdriver.Chrome()
#访问糯米电影票房页
driver.get('http://dianying.nuomi.com/movie/boxoffice')
time.sleep(2)
#打开日历
driver.find_element_by_class_name("calendar-togger").click()
time.sleep(2)
#到达指定开始日期
for p in range(0,1):
    driver.find_element_by_xpath(".//*[@class='calendar']/table/caption/a[2]").click()
time.sleep(2)

movielist=[0]*10000#电影库总表（不重不漏）
listsize=0#电影总数
tip=0#查重标志
#开始爬取票房数据
for x in range(2, 3):#月循环：在这里设定为爬取上个月数据
    for y in range(1, 7):#周循环
        for z in range(1, 8):#日循环
            try:
                #定位到某一天
                driver.find_element_by_xpath(".//*[@class='calendar']/table/tbody/tr[" + str(y) + "]/td[" + str(z) + "]").click()
                time.sleep(2)
                day = driver.find_element_by_id("selectedDay").text
                #爬取核心数据datas，并进行数据挖掘
                datas = driver.find_elements_by_xpath(".//*[@class='movie-table']/dd/div")
                value=[0]*11
                for i in range(0,len(datas)):
                    value[i%11] = datas[i].text
                    if i%11==0:
                        movieid=datas[i].get_attribute('to-url')
                        movieid=movieid[movieid.find('=')+1:len(movieid)]
                        #查重
                        for index in range(0,listsize):
                            if movielist[index]==movieid:
                                tip=1
                                break
                        if tip==0:
                            movielist[listsize]=movieid
                            listsize+=1
                        tip=0#查重结束，标志归位，等待下次查重
                        name=value[0].split( )[0]
                    i=i+1
                    if i%11==0:
                        #数据入库
                        sql_insert = "insert into ticket(day,movieid,name,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10) values('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');"%(day,movieid,name,value[1],value[2],value[3],value[4],value[5],value[6],value[7],value[8],value[9],value[10])
                        sql_insert.encode('utf-8')
                        cur.execute(sql_insert)
                        db.commit()
                driver.find_element_by_class_name("calendar-togger").click()
                time.sleep(2)
            except:
                break
    driver.find_element_by_xpath(".//*[@class='calendar']/table/caption/a[3]").click()#下一月：日历翻页
    time.sleep(2)
driver.quit()#第一阶段完成：票房爬取完毕

#爬虫第二阶段开始：
print("接下来开始爬取有票房信息的电影详情")
urlhead="http://dianying.nuomi.com/movie/detail?movieId="
for n in range(0,listsize):
    url=urlhead+movielist[n]#通过拼装法获取每一部电影的介绍页url
    head={'User-Agent': 'User-Agent:Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1'}
    print(url)
    response=requests.get(url,headers=head)#使用传统requests方法，爬取电影介绍页（head伪装成火狐浏览器，否则会得到手机版网页内容）
    #进行数据挖掘
    tree = html.fromstring(response.text)
    s0 = tree.xpath('//*[@id="detailIntro"]/div/div[2]/h4/text()')
    for s in s0:
        name=s
    s1 = tree.xpath('//*[@id="detailIntro"]/div/div[2]/div[2]/p[2]/text()')
    for s in s1:
        daoyan=s.split( )[0]
        daoyan=daoyan[3:len(daoyan)]
        zhuyan=s.split( )[1]
        zhuyan=zhuyan[3:len(zhuyan)]
    s2 = tree.xpath('//*[@id="detailIntro"]/div/div[2]/div[2]/p[3]/text()')
    for s in s2:
        diqu=s.split( )[0]
        shichang=s.split( )[2]
        ontime=s.split( )[3]
    #数据入库
    sql_insert = "insert into movie(movieid,name,b1,b2,b3,b4,b5) values('%s','%s','%s','%s','%s','%s','%s');"%(movielist[n],name,daoyan,zhuyan,diqu,shichang,ontime)
    sql_insert.encode('utf-8')
    cur.execute(sql_insert)
    db.commit()
print("爬取完毕")
