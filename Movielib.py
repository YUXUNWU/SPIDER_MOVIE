#coding=utf-8
#头文件：导入所需库
from flask import Flask,request
import importlib,sys
import numpy as np
import matplotlib.pyplot as plt
from plotly.offline.offline import matplotlib
importlib.reload(sys)
import pymysql
from flask.templating import render_template
from pip._vendor.requests.api import post
from pylab import mpl

#MySQL数据库链接
db = pymysql.connect(host="localhost", user="root", password="MYSQL2015190103", db="spider_movie", port=3306, use_unicode=True, charset="utf8")
cur = db.cursor()
#启用后端flask框架
app = Flask(__name__)

#路由一
@app.route('/')
def hello_world():
    cur.execute("SELECT * FROM ticket")
    datarows = cur.fetchall()
    return render_template("index.html",datas=datarows)#渲染到index.html的jinja2

#路由二
@app.route('/detail',methods=['GET','POST'])
def getdetail():
    movieid=request.args.get("movieid");
    cur.execute("SELECT * FROM movie where movieid='%s'"%movieid)
    rows = cur.fetchall()
    row=rows[0]
    cur.execute("SELECT * FROM ticket where movieid='%s'"%movieid)
    tips = cur.fetchall()
    x=[0]*365
    y=[0]*365
    length=0
    for tip in tips:
        x[length]=tip[1]
        temp=tip[4]
        y[length]=temp[0:len(tip)]
        length+=1
    for i in range(length,365):
        x[i]=x[i-1]
        y[i]=y[i-1]
    #绘图
    plt.figure(figsize=(12,7))
    mpl.rcParams['font.sans-serif'] = ['SimHei']
    plt.plot(x,y,"b--",linewidth=1)
    plt.xticks(x, x, rotation=80)
    plt.margins(0)
    plt.xlabel("日期")
    plt.ylabel("票房")
    plt.title("电影每日票房折线图")
    #保存图片
    src="./static/example"+movieid+".png"
    plt.savefig(src)
    return render_template("detail.html",detail=row)#渲染到detail.html的jinja2



#启动后端
if __name__ == '__main__':
    app.run(host='0.0.0.0',port=80,debug=True)
